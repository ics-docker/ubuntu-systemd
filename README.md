Docker Ubuntu Systemd
=====================

This Dockerfile builds a Ubuntu based docker systemd capable container.

The main purpose of this image is to allow testing of Ansible roles that
require systemd module to start, stop and see the status of a daemon.

This image is built using GitLab CI and provides a container image 
using the local container image repository.
